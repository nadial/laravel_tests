<?php

use Illuminate\Database\Seeder;

class ModuleStudySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 1
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 2
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 3
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 4
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 5
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 6
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 7
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 8
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 9
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 10
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 11
        ]);
        DB::table('module_study')->insert([
            'study_id' => 1,
            'module_id' => 12
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 1
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 2
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 3
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 4
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 5
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 6
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 7
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 12
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 13
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 14
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 15
        ]);
        DB::table('module_study')->insert([
            'study_id' => 2,
            'module_id' => 16
        ]);

    }
}
