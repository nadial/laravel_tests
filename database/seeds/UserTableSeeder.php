<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
            'name' => 'alumno',
            'rol' => 'alumno',
            'surname' => 'García López',
            'email' => 'alumno@gmail.com',
            'password' => bcrypt('alumno'),
        ]);
        DB::table('users')->insert([
            'name' => 'admin',
            'rol' => 'admin',
            'surname' => 'García López',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
        ]);
        DB::table('users')->insert([
            'name' => 'profesor',
            'rol' => 'profesor',
            'surname' => 'Sánchez Moreno',
            'email' => 'profesor@gmail.com',
            'password' => bcrypt('profesor'),
        ]);
    }
}
