<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            'category_id' => 1,
            'module_id' => 11,
            'a' => 'ports.conf',
            'b' => 'server.xml',
            'c' => 'apache2.conf',
            'd' => 'web.xml',
            'solution' => 'c',
            'wording' => '¿Cómo se llama el archivo de configuración de Apache?',
        ]);
        DB::table('questions')->insert([
            'category_id' => 2,
			 'module_id' => 8,
            'a' => 'Automatic Java Auto-installed eXtension',
            'b' => 'Asynchronous Java and XML',
            'c' => 'Asynchronous Javascript and XML',
            'd' => 'Automatic Javascript and XML',
            'solution' => 'c',
            'wording' => '¿Qué significa AJAX?',
        ]);
        DB::table('questions')->insert([
            'category_id' => 3,
			'module_id' => 10,
            'a' => 'br',
            'b' => 'hr',
            'c' => 'line',
            'd' => 'comment',
            'solution' => 'b',
            'wording' => '¿Qué etiqueta utilizamos para insertar una línea horizontal?',
        ]);
        DB::table('questions')->insert([
            'category_id' => 4,
			'module_id' => 9,
            'a' => 'Crear un fichero normal',
            'b' => 'Crear un enlace simbólico entre dos ficheros',
            'c' => 'Contar el número de segmentos que componen un fichero',
            'd' => 'Eliminar un fichero',
            'solution' => 'd',
            'wording' => 'En PHP, la función unlink() se emplea para',
        ]);
        DB::table('questions')->insert([
            'category_id' => 5,
            'module_id' => 12,
            'a' => 'El modo de actuar sistemáticamente una empresa dentro de su entorno',
            'b' => 'La representación gráfica de la estructura y organización de una empresa',
            'c' => 'Conjunto de elementos interrelacionados entre sí y con el sistema económico global, diseñados para alcanzar un objetivo específico',
            'd' => 'Conjunto de departamentos y divisiones de la empresa',
            'solution' => 'c',
            'wording' => '¿Qué significa "sistema empresarial"?',
        ]);
        DB::table('questions')->insert([
            'category_id' => 5,
            'module_id' => 12,
            'a' => 'Organización general y específica',
            'b' => 'Organización general, democrática y autoritaria',
            'c' => 'Organización vertical y horizontal',
            'd' => 'Organización vertical, horizontal y circular',
            'solution' => 'c',
            'wording' => 'Atendiendo al principio de jerarquía y división del trabajo, podemos distinguir los siguientes modelos de organización empresarial:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 5,
            'module_id' => 12,
            'a' => 'Entorno estable/dinámico, simple/complejo y hostil/favorable',
            'b' => 'Entorno general y entorno específico',
            'c' => 'Entorno estable  o dinámico',
            'd' => 'Entorno vertical, horizontal y específico',
            'solution' => 'b',
            'wording' => 'En función de la naturaleza del entorno en el que se desenvuelve la empresa, distinguimos las siguientes clases de entorno:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 5,
            'module_id' => 12,
            'a' => 'Entorno estable/dinámico, simple/complejo, hostil/favorable',
            'b' => 'Entorno general y específico',
            'c' => 'Entorno dinámico/complejo, estable/hostil, simple/favorable',
            'd' => 'Entorno estable/complejo, dinámico/hostil, favorable/complejo',
            'solution' => 'a',
            'wording' => 'En función de las características de los cambios que se originan en los diferentes factores a lo largo del tiempo, distinguimos las siguientes clases de entorno:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 5,
            'module_id' => 12,
            'a' => 'Competencia actual y futura, productos sustitutivos, proveedores y clientes',
            'b' => 'Factores políticos, legales, demográficos y económicos',
            'c' => 'El envejecimiento de la población, la inflación, la deflación y el cambio climático',
            'd' => 'Factores socioculturales, tecnológicos y medioambientales',
            'solution' => 'a',
            'wording' => 'El entorno específico está formado por los siguientes factores:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 5,
            'module_id' => 12,
            'a' => 'Deliberaciones, atenciones, fracasos y omisiones',
            'b' => 'Decisiones, amenazas, fortalezas y oportunidades',
            'c' => 'Debilidades, amenazas, fortalezas y oportunidades',
            'd' => 'Desarrollo, ambición, familiaridad y ocasión',
            'solution' => 'c',
            'wording' => 'La palabra DAFO corresponde a las iniciales de cuatro palabras:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 5,
            'module_id' => 12,
            'a' => 'El desarrollo económico, el entorno y las ventas',
            'b' => 'Económica, sociocultural y medioambiental',
            'c' => 'Cumplir las leyes, respetar los derechos humanos y procurar la distribución de la riqueza',
            'd' => 'Los derechos de los trabajadores, los derecho humanos y los derecho de la sociedad',
            'solution' => 'b',
            'wording' => 'La responsabilidad social de la empresa está conformada por tres áreas básicas. ¿Sabrías decir cuáles?:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 5,
            'module_id' => 12,
            'a' => 'La formación cultural de sus directivos',
            'b' => 'Lo que se percibe de una empresa, lo que significa',
            'c' => 'Lo que identifica la forma de ser de una empresa su forma de pensar, de vivir y actuar',
            'd' => 'Ninguna es cierta',
            'solution' => 'c',
            'wording' => '¿Qué entiendes por cultura empresarial?',
        ]);
        DB::table('questions')->insert([
            'category_id' => 3,
            'module_id' => 10,
            'a' => 'Un organismo para hacer sitios web',
            'b' => 'Una disciplina que estudia la interacción entre personas y máquinas',
            'c' => 'Una publicación sobre avances en el campo de la interacción entre personas y máquinas',
            'd' => null,
            'solution' => 'b',
            'wording' => '¿Qué es la IPO?',
        ]);
        DB::table('questions')->insert([
            'category_id' => 3,
            'module_id' => 10,
            'a' => 'Es la separación entre el borde de esa caja y otras cajas adyacentes',
            'b' => 'Es la separación entre el borde de esa caja y los elementos que hay en su interior',
            'c' => 'Se puede decir que es el ancho y alto de la caja',
            'd' => null,
            'solution' => 'b',
            'wording' => 'El padding en el modelo de cajas:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 3,
            'module_id' => 10,
            'a' => 'Se puede definir como position:absolute',
            'b' => 'Se puede definir como position:fixed',
            'c' => 'Se puede definir como relative:no_movement',
            'd' => null,
            'solution' => 'b',
            'wording' => 'Si se desea poner un elemento que no se oculte moviendo las barras de desplazamiento:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 3,
            'module_id' => 10,
            'a' => 'Clear elimina el efecto del float',
            'b' => 'Los elementos de una página se posicionan por defecto como absolute',
            'c' => 'El posicionamiento relativo permite que un bloque esté fuera de su posición normal',
            'd' => 'El posicionamiento absoluto te permite situar un elemento en cualquier lugar de la página, indicando la distancia en píxeles.',
            'solution' => 'b',
            'wording' => 'Respecto a la posición de las cajas es falso que:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 3,
            'module_id' => 10,
            'a' => 'Creative Commons es la implementación del copyleft anglosajón',
            'b' => 'Copyright atiende a los derechos morales de los autores.',
            'c' => 'Creative Commons permite diferentes modelos de licencias con diferentes niveles de restricción',
            'd' => null,
            'solution' => 'c',
            'wording' => 'Respecto a las licencias de software:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 3,
            'module_id' => 10,
            'a' => 'En la web predominan las animaciones Flash. aunque con HTML5 se empiezan a utilizar menos ya que se ofrece como una alternativa',
            'b' => 'El problema de Flash para animaciones es que no permite hacer efectos tan vistos como HTML5',
            'c' => 'Flash es siempre la mejor alternativa para hacer animaciones para un sitio web',
            'd' => null,
            'solution' => 'a',
            'wording' => 'Respecto a las animaciones:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Es una unidad de medida relativa',
            'b' => 'Es una unidad de medida absoluta',
            'c' => 'Es una unidad de medida obsoleta',
            'd' => null,
            'solution' => 'a',
            'wording' => 'Em',
        ]);
        DB::table('questions')->insert([
            'category_id' => 3,
            'module_id' => 10,
            'a' => 'Tiene como finalidad captar y dirigir la mirada del observador',
            'b' => 'Tiene como finalidad el equilibrio visual.',
            'c' => 'Responde a criterios creativos y artísticos',
            'd' => 'Responde a criterios geométricos',
            'solution' => 'a',
            'wording' => 'La “Tensión compositiva”',
        ]);
        DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'No, hace falta javaScript',
            'b' => 'Sí mediante pseudo-elementos',
            'c' => 'Sí mediante pseudo-clases',
            'd' =>  null,
            'solution' => 'a',
            'wording' => '¿Mediante css se puede cambiar la primera letra de cada párrafo?',
        ]);
        DB::table('questions')->insert([
            'category_id' => 3,
            'module_id' => 10,
            'a' => '.png',
            'b' => '.gif',
            'c' => '.jpeg',
            'd' =>  '.flash',
            'solution' => 'a',
            'wording' => 'Si queremos mostrar imágenes renderizadas el formato optimo es:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Independientes de los navegadores, todos las soportan',
            'b' => 'Objetos java para incrustar',
            'c' => 'Funciones jquery para mover elementos',
            'd' =>  'Ninguna de las anteriores',
            'solution' => 'a',
            'wording' => 'Las animaciones CSS3 son:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Porciones de tiempo en que se divide una animación',
            'b' => 'Evento al pulsar sobre un frame',
            'c' => 'Las teclas de la parte derecha del teclado',
            'd' =>  'Cargan páginas en divisiones del navegador',
            'solution' => 'a',
            'wording' => 'Keyframes son:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'No permitir elementos flotantes',
            'b' => 'Limpiar las variables css',
            'c' => 'Redefinir contadores',
            'd' =>  'Actualizar la caché',
            'solution' => 'a',
            'wording' => 'En css ‘clear’ se utiliza para:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Una librería para compilar código css',
            'b' => 'Una herramienta para encontrar errores en el código css',
            'c' => 'Una librería utiliza por sus colecciones de icono.',
            'd' => 'Ninguna de las anteriores',
            'solution' => 'b',
            'wording' => 'CSS Lint es:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'El tipo de comillas simple',
            'b' => 'Iconos grades para tablets',
            'c' => 'Una colección de imágenes en una sola imagen',
            'd' => null,
            'solution' => 'c',
            'wording' => 'Los Sprites son:',
        ]);
        DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Prueba',
            'b' => 'Prueba',
            'c' => 'Prueba',
            'd' => null,
            'solution' => 'c',
            'wording' => 'Prueba',
        ]);
        DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Prueba',
            'b' => 'Prueba',
            'c' => 'Prueba',
            'd' => null,
            'solution' => 'c',
            'wording' => 'Prueba',
        ]);DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Prueba',
            'b' => 'Prueba',
            'c' => 'Prueba',
            'd' => null,
            'solution' => 'c',
            'wording' => 'Prueba',
        ]);DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Prueba',
            'b' => 'Prueba',
            'c' => 'Prueba',
            'd' => null,
            'solution' => 'c',
            'wording' => 'Prueba',
        ]);DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Prueba',
            'b' => 'Prueba',
            'c' => 'Prueba',
            'd' => null,
            'solution' => 'c',
            'wording' => 'Prueba',
        ]);DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Prueba',
            'b' => 'Prueba',
            'c' => 'Prueba',
            'd' => null,
            'solution' => 'c',
            'wording' => 'Prueba',
        ]);DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Prueba',
            'b' => 'Prueba',
            'c' => 'Prueba',
            'd' => null,
            'solution' => 'c',
            'wording' => 'Prueba',
        ]);DB::table('questions')->insert([
            'category_id' => 6,
            'module_id' => 10,
            'a' => 'Prueba',
            'b' => 'Prueba',
            'c' => 'Prueba',
            'd' => null,
            'solution' => 'c',
            'wording' => 'Prueba',
        ]);

    }
}
