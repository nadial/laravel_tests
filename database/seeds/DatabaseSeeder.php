<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call(StudiesTableSeeder::class);
      $this->call(ModuleTableSedder::class);
      $this->call(ModuleStudySeeder::class);
      $this->call(CategoryTableSeeder::class);
      $this->call(QuestionTableSeeder::class);
      $this->call(UserTableSeeder::class);
      $this->call(CriterionTableSeeder::class);
      
    }
}
