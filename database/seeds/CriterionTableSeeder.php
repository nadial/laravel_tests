<?php

use Illuminate\Database\Seeder;

class CriterionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('criterions')->insert([
            'name' => 'Cada 2 fallos -0.5',
            'fallos' => 2,
            'descuento' => 0.5
        ]);
        DB::table('criterions')->insert([
            'name' => 'Cada fallo -0.1',
            'fallos' => 1,
            'descuento' => 0.1
        ]);
        DB::table('criterions')->insert([
            'name' => 'Cada 4 fallos -1',
            'fallos' => 4,
            'descuento' => 1
        ]);
    }
}
