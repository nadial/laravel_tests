<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Aplicaciones Web',
            'module_id' => 11
        ]);
        DB::table('categories')->insert([
           'name' => 'Ajax',
           'module_id' => 8
        ]);
        DB::table('categories')->insert([
            'name' => 'Html',
            'module_id' => 10
        ]);
        DB::table('categories')->insert([
            'name' => 'Php',
            'module_id' => 9
        ]);
        DB::table('categories')->insert([
            'name' => 'Empresa y Sociedad',
            'module_id' => 12
        ]);
        DB::table('categories')->insert([
            'name' => 'CSS',
            'module_id' => 1
        ]);
    }
}
