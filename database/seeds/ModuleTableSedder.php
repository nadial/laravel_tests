<?php

use Illuminate\Database\Seeder;

class ModuleTableSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'name' => 'Lenguaje de Marcas',
            'code' => '3031',
            'shortName' => 'MARCAS'
        ]);
        DB::table('modules')->insert([
            'name' => 'Programacion',
            'code' => '3032',
            'shortName' => 'PROG'
        ]);
        DB::table('modules')->insert([
            'name' => 'Bases de Datos',
            'code' => '3033',
            'shortName' => 'BBDD'
        ]);
        DB::table('modules')->insert([
            'name' => 'Sistemas Informáticos',
            'code' => '3034',
            'shortName' => 'SIST'
        ]);
        DB::table('modules')->insert([
            'name' => 'Ingles',
            'code' => '3035',
            'shortName' => 'ING'
        ]);
        DB::table('modules')->insert([
            'name' => 'Formación y Orientación Laboral',
            'code' => '3036',
            'shortName' => 'FOL'
        ]);
        DB::table('modules')->insert([
            'name' => 'Entornos de Desarrollo',
            'code' => '3037',
            'shortName' => 'ED'
        ]);
        DB::table('modules')->insert([
            'name' => 'Desarrollo en entorno cliente',
            'code' => '3038',
            'shortName' => 'DC'
        ]);
        DB::table('modules')->insert([
            'name' => 'Desarrollo en entorno servidor',
            'code' => '3039',
            'shortName' => 'DS'
        ]);
        DB::table('modules')->insert([
            'name' => 'Diseño de Intefaces web',
            'code' => '3040',
            'shortName' => 'DW'
        ]);
        DB::table('modules')->insert([
            'name' => 'Despliegue de aplicaciones',
            'code' => '3041',
            'shortName' => 'DA'
        ]);
        DB::table('modules')->insert([
            'name' => 'Empresa e Iniciativa Empresarial',
            'code' => '3042',
            'shortName' => 'EIE'
        ]);
        DB::table('modules')->insert([
            'name' => 'Programación de servicios y procesos',
            'code' => '3021',
            'shortName' => 'PSP'
        ]);
        DB::table('modules')->insert([
            'name' => 'Programación multimedia y dispositivos móviles',
            'code' => '3022',
            'shortName' => 'PMDM'
        ]);
        DB::table('modules')->insert([
            'name' => 'Desarrollo de interfaces',
            'code' => '3023',
            'shortName' => 'DI'
        ]);
        DB::table('modules')->insert([
            'name' => 'Acceso a datos',
            'code' => '3024',
            'shortName' => 'AD'
        ]);
    }
}
