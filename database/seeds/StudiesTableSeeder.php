<?php

use Illuminate\Database\Seeder;

class StudiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('studies')->insert([
            'name' => 'Desarrollo de Aplicaciones WEB',
            'code' => '303',
            'shortName' => 'DAW'
        ]);
        DB::table('studies')->insert([
            'name' => 'Desarrollo de Aplicaciones Multiplataforma',
            'code' => '302',
            'shortName' => 'DAM'
        ]);
        DB::table('studies')->insert([
            'name' => 'Sistemas Microinformáticos y redes',
            'code' => '201',
            'shortName' => 'SMR'
        ]);
    }
}
