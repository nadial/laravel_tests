<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table){
            $table->increments('id');
            $table->integer('module_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('wording', 250);
            $table->string('a', 250);
            $table->string('b', 250);
            $table->string('c', 250);
            $table->string('d', 250)->nullable();
            $table->string('solution', 1);
            $table->timestamps();
            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('category_id')->references('id')->on('categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
