<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table){
            $table->increments('id');
            $table->integer('module_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('criterion_id')->unsigned()->nullable();
            $table->string('name', 150);
            $table->integer('time')->nullable();
            $table->integer('num_preguntas');
            $table->timestamps();
            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('criterion_id')->references('id')->on('criterions');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}