<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDotestQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('do_test_question', function (Blueprint $table){
                $table->increments('id');
                $table->integer('question_id')->unsigned();
                $table->integer('do_test_id')->unsigned();
                $table->foreign('do_test_id')->references('id')->on('do_tests')->onDelete('cascade');
                $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
                $table->string('contestada', 1);
                $table->string('acertada', 2);
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('do_test_question');
    }
}
