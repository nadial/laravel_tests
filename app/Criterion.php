<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criterion extends Model
{
	protected $table = 'criterions';
    public function tests()
    {
        return $this->hasMany('App\Test');
    }
}
