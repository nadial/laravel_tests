<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = ['name', 'code', 'shortName'];

    public function tests()
    {
        return $this->hasMany('App\Test');
    }
    public function questions()
    {
        return $this->hasMany('App\Question');
    }
    public function studies()
    {
        return $this->belongsToMany('App\Study');
    }
    public function categories()
    {
        return $this->hasMany('App\Category');
    }
}
