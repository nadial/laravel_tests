<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoTest extends Model
{
    public function test()
    {
        return $this->belongsTo('App\Test');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function questions()
    {
        return $this->belongsToMany('App\Question')->withPivot('contestada', 'acertada');
    }
}
