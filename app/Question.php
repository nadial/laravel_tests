<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function tests()
    {
        return $this->belongsToMany('App\Test');
    }
    public function modules()
    {
        return $this->belongsTo('App\Module');
    }
    public function dotest()
    {
        return $this->belongsToMany('App\DoTest')->withPivot('contestada');
    }
}
