<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','rol','surname'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function tests()
    {
        return $this->hasMany('App\Test');
    }
    public function dotests()
    {
        return $this->hasMany('App\DoTest');
    }
    public function isAlumn()
    {
        return $this->rol == 'alumno';
    }
    public function isTeacher()
    {
        return $this->rol == 'profesor';
    }
    public function isAdmin()
    {
        return $this->rol == 'admin';
    }
}
