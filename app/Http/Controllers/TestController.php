<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\DoTest;
use App\Module;
use App\Study;
use App\Question;
use App\Criterion;
use PDF;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $studies = Study::all();
        $criterions = Criterion::all();
        if (! $user->can('create', Test::class)) {
            return redirect('/tests/mostrar'); 
        }
        return view('test.index', ['studies'=> $studies, 'criterions'=> $criterions]);

    }
    public function mostrar(Request $request)
    {
        $tests = Test::all();

        return view('test.tests', ['tests'=> $tests]);

    }
    public function ajax($id)
    {
        $study = Study::find($id);
        return json_encode($study->modules()->get());
    }
    public function ajaxquestion($id)
    {
        $module = Module::find($id);
        return json_encode($module->questions()->get());
    }
    public function comprobar($id, Request $request){
        $test = Test::find($id);
        $user = $request->user();
        $dotest = new DoTest;
        $dotest->test_id = $id;
        $dotest->user_id = $user->id;
        $dotest->acertadas = 0;
        $dotest->falladas = 0;
        $dotest->save();
        foreach ($test->questions()->get() as $question) {
            if ($question->solution == $request->input("$question->id")){
                $dotest->questions()->attach($question->id, ['contestada' => $request->input("$question->id"), 'acertada' => "si",]);
            }else{
                $dotest->questions()->attach($question->id, ['contestada' => $request->input("$question->id"), 'acertada' => "no",]);
            }
        }
        $resultado = $this->resultado($dotest);
        $dotest->acertadas = $resultado['acertadas'];
        $dotest->falladas = $resultado['falladas'];
        $dotest->save();
        
        return redirect("/tests/hecho/$dotest->test_id");
        /*return view('test.resultado', ['nota' => $dotest->nota, 'fallos' => $contfallos]);*/
    }
    public function doTestall($id){
        $this->authorize('create', Test::class);
        $test = Test::find($id);
        return view('test.dotest', ['test'=> $test]);
    }
    public function resultado(DoTest $dotest){
        $contaciertos = 0;
        $contfallos = 0;
        $test = Test::find($dotest->test_id);
        $criterion = $test->criterion()->first();
        foreach ($test->questions()->get() as $question) {
            foreach ($dotest->questions()->get() as $questionRes) {
                if ($question->id == $questionRes->id) {
                    if ($question->solution == $questionRes->pivot->contestada) {
                        $contaciertos++;
                    }else{
                        $contfallos++;
                    }
                }
            }
        }
        $resultado['acertadas'] = $contaciertos;
        $resultado['falladas'] = $contfallos;
        return $resultado;
    }
    public function calcular(DoTest $dotest){
        $test = $dotest->test()->first();

        if ($test->criterion_id != null){
            $criterion = $test->criterion()->first();
            $cont = 0;
        $descuento = 0;
        for ($i = 1 ; $i <= $dotest->falladas; $i++){
            $cont++;
            if ($cont == $criterion->fallos){
                $cont = 0;
                $descuento = $descuento + $criterion->descuento;
            }
            
        }
        }else{
            $descuento = 0;
        }
        
        
        $nota = (($dotest->acertadas*10)/$test->num_preguntas)-$descuento;
        return $nota;
    }
    public function hecho($id, Request $request){
        $test = Test::find($id);
        $user = $request->user();
        $dotest = $test->dotests()->where('user_id', '=', $user->id)->first();
        $nota = $this->calcular($dotest);
        return view('test.hecho', ['dotest'=> $dotest, 'nota'=> $nota]);
        
    }
    public function hacer($id)
    {
        $test = Test::findOrFail($id);
        return view('test.hacer', ['test'=> $test]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('test.create', ['study'=> $study]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $idquestions = $request->input('preguntas');
        $numero = count($idquestions);
        $test = new Test;
        $test->module_id = $request->input('module');
        $test->user_id = $user->id;
        $test->num_preguntas = $numero;
        $test->name = $request->input('name');
        if ($request->input('criterion') != 0){
            $test->criterion_id = $request->input('criterion');
        }else{
            $test->criterion_id = null;
        }
        if ($request->input('time') == null || $request->input('time') == 0 || $request->input('time') == ""){
            $test->time = null;
        }else{
            $test->time = $request->input('time');
        }
        
        $test->save();
        foreach ($idquestions as $idquestion) {
            $test->questions()->attach($idquestion);
        }
        return redirect('/tests/'.$test->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $test = Test::findOrFail($id);

        return view('test.show', ['test'=> $test]);

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $families = Family::findOrFail($id);

        return view('family.edit', ['family'=> $families]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [
        'code' => 'required|unique:families|max:4',
        'name' => 'required|max:40'
        ]);
        $family = Family::find($id);
        $family->code = $request->code;
        $family->name = $request->name;
        $family->save();
        return redirect('/families');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Test::find($id)->delete();
    }

}
