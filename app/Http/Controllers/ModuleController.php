<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Module;
use App\Study;

class ModuleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $modules = Module::all();
         if($request->ajax()){
            return $modules;
        }else{

        return view('module.index' , ['modules'=> $modules]);

    }
}
    public function ajaxModules()
    {
        $modules = Module::paginate(10);
        return json_encode($modules);
    }
    public function studies(Request $request){
        $module = Module::find($request->module_id);
        $moduleStudies = $module->studies()->select('study_id')->get();
        foreach($moduleStudies as $moduleStudy){
            $id[] = "$moduleStudy->study_id";
        }
        $studies = Study::whereNotIn('id', $id)->get();
        return json_encode($studies);
        
       
    }
    public function anadir($id, Request $request){
        $study = Study::find($request->input('estudio'));
        $study->modules()->attach($id);
        return redirect('/modules');
    }

    public function buscar($opcion, $name){
        $modules = DB::table('modules')->where("$opcion", 'like' , "$name%" )->paginate(10);
        return json_encode($modules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('module.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
        'name' => 'required|max:30',
        'code' => 'required|unique:modules|max:4',
        'shortName' => 'required|max:10'
        
        ]);

        $module = new Module($request->all());
        $module->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/modules');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $modules = Module::findOrFail($id);
        if ($request->ajax()) {
            return $modules;
        } else {

            return view('module.show', ['module'=> $modules]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modules = Module::findOrFail($id);

        return view('module.edit', ['module'=> $modules]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [
        'code' => 'required|max:4',
        'name' => 'required|max:40'
        ]);
        $module = Module::find($id);
        $module->name = $request->name;

        if ($module->code != $request->code ) {
            $module->code = $request->code;
        }

        $module->shortName = $request->shortName;
        $module->save();
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Module::find($id)->delete();
    }
}

