<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\Module;
use App\Study;
use Illuminate\Support\Facades\DB;

class StudyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $studies = Study::paginate(10);
        if($request->ajax()){
            return json_encode($studies);
        }else{

        return view('study.index' , ['studies'=> $studies]);
    }
}

    public function ajaxStudies()
    {
        $studies = Study::paginate(10);
        return json_encode($studies);
    }

    
    
    public function buscar($name){
        $study = DB::table('studies')->where('name', 'like' , "$name%" )->paginate(10);;
        return json_encode($study);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $study = new Study;

        $study->name = $request->input('name');
        $study->code = $request->input('code');
        $study->shortName = $request->input('shortName');
        $study->save();

        return redirect('/studies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $study = Study::findOrFail($id);

        return view('study.show', ['study'=> $study]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'code' => 'required|max:4',
        'name' => 'required|max:50'
        ]);
        $study = Study::find($id);
        $study->name = $request->name;

        if ($study->code != $request->code ) {
            $study->code = $request->code;
        }

        $study->shortName = $request->shortName;
        $study->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Study::find($id)->delete();
    }
}
