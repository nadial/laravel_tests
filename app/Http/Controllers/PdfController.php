<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;

class PdfController extends Controller
{
    public function invoice($id) 
    {
        $test = $this->getData($id);
        $view =  \View::make('pdf.Alumnos', compact('test'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('Examen_Alumnos');
    }
 
    public function getData($id) 
    {
    	$test = Test::findOrFail($id);
        $data = $test;
        return $data;
    }
}
