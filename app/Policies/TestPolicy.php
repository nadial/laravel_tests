<?php

namespace App\Policies;

use App\User;
use App\Test;
use App\DoTest;
use Illuminate\Auth\Access\HandlesAuthorization;

class TestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the product.
     *
     * @param  \App\User  $user
     * @param  \App\Product  $test
     * @return mixed
     */
    public function view(User $user, Test $test)
    {
        return true;
    }

    /**
     * Determine whether the user can create products.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->isTeacher() || $user->isAdmin()){
            return true;
        }
        return false;
        
    }

    /**
     * Determine whether the user can update the product.
     *
     * @param  \App\User  $user
     * @param  \App\Product  $product
     * @return mixed
     */
    public function update(User $user, Test $test)
    {
        if ($user->id == $test->user_id || $user->rol == 'admin' ) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the product.
     *
     * @param  \App\User  $user
     * @param  \App\Product  $product
     * @return mixed
     */
    public function delete(User $user, Test $test)
    {
        if ($user->id == $test->user_id || $user->rol == 'admin' ) {
            return true;
        }
        return false;
    }
    public function show(User $user, Test $test)
    {
        if ($user->rol == 'admin' ) {
            return true;
        }
        return false;
    }
    public function hacer(User $user, Test $test)
    {
        if ($user->rol == 'alumno') {
            return true;
        }
        return false;
    }

}
