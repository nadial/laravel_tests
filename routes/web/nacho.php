<?php 
Route::post('register/profe', 'Auth\RegisterController@profe');
Route::post('register/alumno', 'Auth\RegisterController@alumno');
Route::get('tests/ajax/{id}', 'TestController@ajax');
Route::post('tests/comprobar/{id}', 'TestController@comprobar');
Route::get('tests/mostrar', 'TestController@mostrar');
Route::get('tests/hacer/{id}', 'TestController@hacer');
Route::get('tests/hecho/{id}', 'TestController@hecho');
Route::get('/', 'TestController@index');
Route::get('/tests/pdf/alumnos/{id}', 'PdfController@invoice');
Route::get('tests/ajaxquestion/{id}', 'TestController@ajaxquestion');
Route::get('tests/hechos/{id}', 'TestController@doTestall');
Route::get('studies/buscar/{name}', 'StudyController@buscar');
Route::get('studies/ajax', 'StudyController@ajaxStudies');
Route::get('modules/buscar/{opcion}/{name}', 'ModuleController@buscar');
Route::get('modules/ajax', 'ModuleController@ajaxModules');
Route::get('modules/studies', 'ModuleController@studies');
Route::post('modules/anadir/{id}', 'ModuleController@anadir');
Route::resource('modules', 'ModuleController');
Route::resource('tests', 'TestController');
Route::resource('studies', 'StudyController');
Route::resource('questions', 'QuestionController');
Route::resource('categories', 'CategoryController');
Route::resource('criterions', 'CriterionController');


?>