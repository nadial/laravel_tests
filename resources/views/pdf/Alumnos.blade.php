<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Example 2</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
    td {
      text-align: center;
      padding: 5px;

    }
    span{
      margin-right: 6em;
    }
    .listapreguntas{
      list-style-type: decimal;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .row.content {height:auto;} 
    }
    </style>
  </head>
  <body>
    <div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-10 text-left"> 
      <h1 style="text-align: center;">Exámen de "{{ $test->module->name }}" </h1>
      <p class="criterio" style="font-style: italic; text-align: center; margin-bottom: 2em;">"(Criterio) Cada 2 fallos -0.5p"</p>
      <div class="datosAlumno" style="float: right;">
        <p>Nombre: ..................................</p>
        <p>Apellidos: ................................</p>
        <p>Curso: ...........</p>
      </div>
      <div class="tablacalificacion" style="margin-left: 70%; margin-bottom: 3em; ">
      <table border="1" style="margin-bottom: 1em; ">
        <tr>
          <td style="padding-right: 2.5em;">Nota:</td><td style="padding-left: 2em;"><h3> /{{ $test->num_preguntas }}</h3></td>
        </tr>
      </table>
      </div>
      <div class="tablarespuestas" style="margin-left: 5%; margin-bottom: 2em; margin-top: 7.7em; width: 5%;">
        <table border="1">
        <?php if ($test->num_preguntas>10 ) {?>
          <tr>
          @for ($i = 0; $i < $test->num_preguntas; $i++)
            
              <th>
                {{ $i+1 }}
              </th>
          @endfor
          </tr>
          <tr>
          @for ($i = 0; $i < $test->num_preguntas; $i++)
              <td style="padding: 0.8em;">
                <span style="margin: 0.5em;  height: 100px;"></span>
              </td> 
          @endfor
          <tr>
        <?php }?>
        <?php if ($test->num_preguntas<=10 ) {?>
        <tr>
          @for ($i = 0; $i < $test->num_preguntas; $i++)
              <td>
                {{ $i+1 }}
              </td>
          @endfor
          </tr>
          <tr>
            @for ($i = 0; $i < $test->num_preguntas; $i++)
              <td style="padding: 0.8em;">
                <span style="margin: 0.5em;  height: 100px;"></span>
              </td>
          @endfor
          </tr>
        <?php }?>
        </table>
      </div>
      <div style="margin-left: 5%;" class="preguntas">
      <?php $cont = 0;?>
      <ol class="listapreguntas">
        @foreach($test->questions as $question)
        <?php $cont++;?>
        <div class="pregunta">
          <li value="{{ $cont }}">
          <div class="enunciado">
            <p><b>Enunciado:</b> {{ $question->wording }}</p>
          </div>
          <div class="respuestas">
            <ul style="list-style-type: lower-latin;" >
              <li>{{ $question->a }}</li>
              <li>{{ $question->b }}</li>
              <li>{{ $question->c }}</li>
              <?php if ($question->d != null) {
                echo "<li> $question->d </li>";
              }?> 
            </ul>
          </div>
          </li>
        </div>
        @endforeach
        </ol>
      </div>
  </body>
</html>