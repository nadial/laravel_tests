@extends('layouts.app')

<meta name="csrf-token" content="{{ csrf_token() }}">

    @section('content')
    <div class="container-fluid text-center">    
    <div class="row content">
    <?php if (Auth::user()->rol == 'admin') { ?>
    <div class="col-sm-2 sidenav">
      <p><a id="crear" href="#" data-toggle="modal" data-target="#myModal">Crear Estudio</a></p> 
    </div>
    <div class="col-sm-10 text-left">
    <?php }else{ ?>
    <div class="col-sm-12">
     <?php } ?>
     
        <h1 class="col-sm-12 text-center">Lista de Estudios</h1>
        <div class="col-sm-9" ></div>       
        <div class="col-sm-3" >
            <div class="form-group has-feedback">
                <input maxlength="30" placeholder="Buscar" class="form-control" type="text" name="name" id="nombreBuscar" "><span style="" class='glyphicon glyphicon-search form-control-feedback'></span>
            </div>   
        </div>
    
        <p id="resultado"></p>

        <table class="table table-condensed col-sm-12" id="tablaStudies" onload="cargarStudies()"> 
        {{ csrf_field() }}
        </table>
        </div>
        <ul class="pagination " id="pagination">

        </ul> 
        </div>
        </div>
        <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear Estudio</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="/studies">
        {{ csrf_field() }}
            <div class="form-group">
                <label class="control-label col-sm-2" for="name">Nombre: </label>
                <div class="col-sm-10">
                <input class="form-control type="text" name="name">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="code">Codigo: </label>
                <div class="col-sm-10">
                <input class="form-control type="text" name="code">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="shortName">Nom. Abreviado: </label>
                <div class="col-sm-10">
                <input class="form-control type="text" name="shortName">
                </div>
            </div>
            <div class="form-group text-center">
            <input class="btn btn-success" type="submit" name="Crear" value="Crear">
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
    var nombre;
    var actualPage;
    var maxPage;
    
    
    function paginador(){
        var ul = document.getElementById("pagination");
        ul.innerHTML = "";
        for (var i = 1; i <=maxPage; i++ ){
            var li = document.createElement("li");
            
            if (actualPage == i) {
                li.setAttribute('class', 'active');
            }

            li.innerHTML = "<a href='#' onclick='cargarStudies("+i+")'>"+i+"</a>";
            ul.appendChild(li);
        }
    }
    function paginadorBuscador(){
        var ul = document.getElementById("pagination");
        ul.innerHTML = "";
        for (var i = 1; i <=maxPage; i++ ){
            var li = document.createElement("li");
            
            if (actualPage == i) {
                li.setAttribute('class', 'active');
            }

            li.innerHTML = "<a href='#' onclick='mostrarStudy("+i+")'>"+i+"</a>";
            ul.appendChild(li);
        }
    }

    function cargarStudies(page){
        var xhttp;
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var tabla = document.getElementById("tablaStudies");
                    tabla.innerHTML = "";
                    var tbody = document.createElement("tbody");
                    tabla.appendChild(tbody);
                    var studies = JSON.parse(this.responseText);
                    maxPage = studies.last_page;
                    actualPage = studies.current_page;
                    var tr = document.createElement("tr");
                    tr.innerHTML = "<th>ID</th><th>Nombre</th><th>Código</th><th>Nom. Abreviado</th><th>Acción</th>";
                    tbody.appendChild(tr);     
                        for (x in studies.data) {
                            var tr = document.createElement("tr");
                            tr.setAttribute('id', 'study');
                            tr.innerHTML += "<td>"+studies.data[x].id+"</td><td>"+studies.data[x].name+"</td><td>"+studies.data[x].code+"</td> <td>"+studies.data[x].shortName+"</td> <td> <a class='btn btn-info' href='/studies/"+studies.data[x].id+"'>Ver</a> <?php if (Auth::user()->rol == 'admin') { ?><a class='btn btn-primary editar' href='#'>Editar</a> <input type='button' class='btn btn-danger' value='Borrar'><?php } ?> </td>"; 
                            tbody.appendChild(tr);
                        }
                        if (maxPage != 1){
                            paginador();
                        }
                        borrarStudy();
                        editarStudy();
                        editado = false;
                }
                if (this.readyState == 4 && this.status == 404){
                    document.getElementById('resultado').innerHTML = "No se han encontrado ningun estudio";
                }
            };
            xhttp.open("GET", "/studies/ajax?page="+page, true);
            xhttp.send();  
            }

    function borrarStudy(){
        $(".btn-danger").click(function(event) {
            var td = this.parentNode;
            var tr = td.parentNode;
            var id = tr.firstChild;
                
            id = id.innerHTML;
            $.ajax({
                url: '/studies/'+id,
                type: 'delete',
                data: {'_token': '<?php echo csrf_token(); ?>'},
                success: function(result){
                    $(tr).fadeOut(2000);
                },
                error: function(){
                    console.log('Error');
                }
            });
        
        });
    }

    var editado = false;
    function editarStudy(){
        $(".editar").click(function(event) {
            if (editado == false) {
            var td = this.parentNode;
            var tr = td.parentNode;
            var id = tr.childNodes[0].innerHTML;
            var nombre = tr.childNodes[1].innerHTML;
            var codigo = tr.childNodes[2].innerHTML;
            var nomAbre = tr.childNodes[4].innerHTML;
            tr.childNodes[1].innerHTML = "<input class='inputEdit' type='text' name='name' value='"+nombre+"'>";
            tr.childNodes[2].innerHTML = "<input class='inputEdit' type='text' name='code' value='"+codigo+"'>";
            tr.childNodes[4].innerHTML = "<input class='inputEdit' type='text' name='shortName' value='"+nomAbre+"'>";
            var td4 = tr.childNodes[6];
            var editar = td4.childNodes[3];
            editar.setAttribute('class', 'btn btn-success editable');
            editar.setAttribute('onclick', 'studyEditado(this)');
            editar.innerHTML = "Aceptar";
            editado = true;
        }
            cambiarInput();
        });

    }
    function cambiarInput(){
        $(".inputEdit").keyup(function(event) {
               var valor = this.value;
               this.setAttribute('value', valor);
            });
    }
    function studyEditado(elemento){
        var td = elemento.parentNode;
        var tr = td.parentNode;
        tr.setAttribute('style', 'background-color: white;');
        var id = tr.childNodes[0].innerHTML;
        var inputNombre = tr.childNodes[1];
        var nombre = inputNombre.firstChild.value;
        var inputCodigo = tr.childNodes[2];
        var codigo = inputCodigo.firstChild.value;
        var inputNomAbre = tr.childNodes[4];
        var nomAbre = inputNomAbre.firstChild.value;
        var td4 = tr.childNodes[6];
        var editar = td4.childNodes[3];
        $.ajax({
                url: '/studies/'+id,
                type: 'put',
                data: {'_token': '<?php echo csrf_token(); ?>', 'name': nombre, 'code': codigo, 'shortName': nomAbre},
                success: function(result){
                    editado = false;
                    inputNombre.innerHTML = nombre;
                    inputCodigo.innerHTML = codigo;
                    inputNomAbre.innerHTML = nomAbre;
                    editar.setAttribute('class', 'btn btn-primary editar');
                    editar.setAttribute('onclick', '');
                    editar.innerHTML = "Editar";
                    $(tr).animate({opacity: '0.4'}, "slow");
                    $(tr).animate({opacity: '1'}, "slow");
                },
                error: function(){
                    console.log('Error');
                }
            });


    }
    

    function mostrarStudy(page) {
            var xhttp;
            var html ="";
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var tabla = document.getElementById("tablaStudies");
                    tabla.innerHTML = "";
                    var tbody = document.createElement("tbody");
                    tabla.appendChild(tbody);
                    var studies = JSON.parse(this.responseText);
                    maxPage = studies.last_page;
                    actualPage = studies.current_page;
                    var tr = document.createElement("tr");
                    tr.innerHTML = "<th>ID</th><th>Nombre</th><th>Código</th><th>Nom. Abreviado</th><th>Acción</th>";
                    tbody.appendChild(tr);     
                        for (x in studies.data) {
                            var tr = document.createElement("tr");
                            tr.innerHTML = "<td>"+studies.data[x].id+"</td><td>"+studies.data[x].name+"</td><td>"+studies.data[x].code+"</td> <td>"+studies.data[x].shortName+"</td> <td> <a class='btn btn-info' href='/studies/"+studies.data[x].id+"'>Ver</a> <?php if (Auth::user()->rol == 'admin') { ?><a class='btn btn-primary editar' href='#'>Editar</a> <input type='button' class='btn btn-danger' value='Borrar'><?php } ?> </td>"; 
                            tbody.appendChild(tr);
                        }
                        if (maxPage != 1){
                            paginadorBuscador();
                        }
                }
                if (this.readyState == 4 && this.status == 404){
                    cargarStudies();
                }
            };
            xhttp.open("GET", "/studies/buscar/"+nombre+"?page="+page, true);
            xhttp.send();  
            }
</script>
<script>
    $(document).ready(function(){
        cargarStudies();
        $("#nombreBuscar").keyup(function(){
            nombre = document.getElementById('nombreBuscar').value;
            mostrarStudy();
        });
        
    });
</script>


    @stop
    