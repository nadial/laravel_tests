@extends('layouts.app')
    @section('content')
    <div class="container-fluid text-center">    
    <div class="row content">

    <div class="col-sm-10 text-left"> 
        <h1 class="text-center">Estudio {{ $study->id }}: <b>{{ $study->name }} ({{ $study->shortName }})</b></h1>

        <h3>Modulos:</h3>
        <ul style="margin-left: 10%;" class="list-group">
            @foreach ($study->modules as $module)
                <li class="list-group-item">{{ $module->name }}</li>
            @endforeach
        </ul>

        <a href="/studies" class="text-center">Volver</a>
    </div>
        
    </div>

@stop