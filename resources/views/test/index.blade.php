@extends('layouts.app')
    @section('content')
    <div class="container-fluid text-center">    
    <div class="row content">
    <div class="col-sm-2 sidenav">
      <p><a href="/questions/create">Crear Preguntas</a></p>
      <p><a href="/tests/mostrar">Ver Examenes</a></p>
      
    </div>
    <div class="col-sm-10 text-left">
        <h1 class="text-center">Crear Exámen</h1>
        <form class="form" action="/tests" method="post">
        {{ csrf_field() }}
            <div class="form-group">
            <label for="sel1">*Nombre: </label>
            <input class="form-control" type="text" name="name">
            </div>
            <div class="form-group">
            <label for="sel1">Tiempo(minutos): </label>
            <input class="form-control" type="text" name="time">
            </div>
            <div class="form-group">
            <label for="sel1">*Criterio: </label>
            <select class="form-control" name="criterion">
                <option value="0">Ninguno</option>
                @foreach($criterions as $criterion)
                    <option value="{{ $criterion->id }}">{{ $criterion->name }}</option>
                @endforeach
            </select><br>
            <a href="/criterions/create">Crear criterio</a>
            </div>
            <div class="form-group">
            <label for="sel1">*Estudio: </label>
            <select class="form-control" name="study" onchange="mostrarmodulos(this.value)">
                <option value="0">--Selecciona Estudio--</option>
                @foreach($studies as $study)
                    <option value="{{ $study->id }}">{{ $study->name }}</option>
                @endforeach
            </select>
            </div>
            <div class="form-group">
            <label for="sel1">*Modulo: </label>
            <select class="form-control" name="module" id="modules" onchange="mostrarpreguntas(this.value)">
            </select>
            </div>
            <div class="form-group">
            <label>*Preguntas:</label><p class="text-center" id="numero"> </p>
            <div id="tablaPreguntas" style="overflow: scroll; border: 1px solid #ddd; height: 500px;  ">
            </div>
            </div>
            <div class="form-group text-center">
                <input class="btn btn-success" type="submit" name="Crear" value="Crear">
            </div>
        </form>
        </div>
        
        </div>

        <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear Módulo</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="/questions">
        {{ csrf_field() }}
            <div class="form-group">
                <label class="control-label col-sm-2" for="name">Enunciado: </label>
                <div class="col-sm-10">
                <input class="form-control type="text" name="name">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="code">Codigo: </label>
                <div class="col-sm-10">
                <input class="form-control type="text" name="code">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="shortName">Nom. Abreviado: </label>
                <div class="col-sm-10">
                <input class="form-control type="text" name="shortName">
                </div>
            </div>
            <input class="btn btn-success" type="submit" name="Crear" value="Crear">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
 <script>
    
            function mostrarmodulos(id) {
            var xhttp;
            var html ="";
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var modules = JSON.parse(this.responseText);
                        for (x in modules) {
                            html += "<option value='"+modules[x].id+"'>"+modules[x].name+"</option>";
                        }
                    
                document.getElementById('modules').innerHTML = html;
                }
                if (this.readyState == 4 && this.status == 500){
                    html += "<option>--Nada--</option>";
                    document.getElementById('modules').innerHTML = html;
                }
            };
            xhttp.open("GET", "/tests/ajax/"+id, true);
            xhttp.send();  
            }
            function mostrarpreguntas(id) {
            var xhttp;
            var html ="";
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var questions = JSON.parse(this.responseText);
                        
                        for (x in questions) {
                            if (x%2==0) {
                                    html += "<div class='col-md-12'>";
                                    html += "<div style='z-index:2; padding:1em;' class='checkbox col-md-2 col-sm-12 text-center'>";
                                        html += "<input name='preguntas[]' id='preguntas' style='margin-left:5px;' onclick='comprobar(this)' type='checkbox' value="+questions[x].id+">";
                                    html += "</div>";
                                    html += "<div style='background-color: #99FFFF ;z-index:1;' class='col-md-10 col-sm-12'>";
                                        html += "<p>"+questions[x].wording+"</p>";
                                        html += "<ul style='list-style-type: lower-latin;'>";
                                            html += "<li>"+questions[x].a+"</li>";
                                            html += "<li>"+questions[x].b+"</li>";
                                            html += "<li>"+questions[x].c+"</li>";
                                            if (questions[x].d != null) {
                                                html += "<li>"+questions[x].d+"</li>";
                                            }
                                        html += "</ul>";
                                    html += "</div>";
                                    html += "</div>";
                            }else{
                                html += "<div class='col-md-12'>";
                                html += "<div style='z-index:2;padding:1em;' class='checkbox col-md-2 col-sm-12 text-center'>";
                                        html += "<input name='preguntas[]' id='preguntas' style='margin-left:5px;' onclick='comprobar(this)' type='checkbox' value="+questions[x].id+">";
                                    html += "</div>";
                                    html += "<div style='z-index:1;' class='col-md-10 col-sm-12'>";
                                        html += "<p>"+questions[x].wording+"</p>";
                                        html += "<ul style='list-style-type: lower-latin;'>";
                                            html += "<li>"+questions[x].a+"</li>";
                                            html += "<li>"+questions[x].b+"</li>";
                                            html += "<li>"+questions[x].c+"</li>";
                                            if (questions[x].d != null) {
                                                html += "<li>"+questions[x].d+"</li>";
                                            }
                                        html += "</ul>";
                                    html += "</div>";
                                    html += "</div>";
                            }
                            html += "<hr style='color:black; padding:1em;'>";
                            
                        }
                    
                document.getElementById('tablaPreguntas').innerHTML = html;
                }
                if (this.readyState == 4 && this.status == 500){
                    html += "<option>--Nada--</option>";
                    document.getElementById('origen').innerHTML = html;
                }
            };
            xhttp.open("GET", "/tests/ajaxquestion/"+id, true);
            xhttp.send();  
            }

</script>
<script>
    var contador=0;
    function comprobar(elemento){
        if( $(elemento).prop('checked') ) {
            contador++;
            
        }else{
            contador--;
        
        }
        document.getElementById('numero').innerHTML = "Seleccionadas: " + contador;
    }
</script>
    @stop
