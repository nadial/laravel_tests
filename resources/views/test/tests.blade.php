@extends('layouts.app')
<meta name="csrf-token" content="{{ csrf_token() }}">
    @section('content')
    <div class="container-fluid text-center">    
  <div class="row content">
  @if(Auth::user()->rol == 'admin')
  <div class="col-sm-2 sidenav">
      <p><a href="/tests"><- Volver</a></p>  
  </div>
  <div class="col-sm-10 text-left">
  @endif
  @if(Auth::user()->rol == 'profesor')
  <div class="col-sm-2 sidenav">
      <p><a href="/tests"><- Volver</a></p>  
  </div>
  <div class="col-sm-10 text-left">
  @endif
  @if(Auth::user()->rol == 'alumno')
  <div class="text-left">
  @endif
    
      <h1 class="text-center">Lista de Exámenes</h1>
      <table class="table table-condensed">
        <tr>
          <th>Id</th><th>Nombre</th><th>Modulo</th><th>Num.Preguntas</th><th>Criterio</th><th>Tiempo</th><th>Creado por</th><th>Acciones</th>
        </tr>
        @foreach($tests as $test)
        <tr>
            <td>{{ $test->id }}</td>
            <td>{{ $test->name }}</td>
            <td>{{ $test->module->name }}</td>
            <td>{{ $test->num_preguntas }}</td>
             <?php if (isset($test->criterion)){ ?> <td>{{ $test->criterion->name }}</td> <?php }else{?> <td>Ninguno</td> <?php }?>
            @if ($test->time == null)
            <td>Ilimitado</td>
            @else
            <td>{{ $test->time }} min.</td>
            @endif
            <td>{{ $test->user->name }} {{ $test->user->surname }}</td>
            <td> 
                @can('delete', $test)
                
                <a class='btn btn-info' href='/tests/hechos/{{ $test->id }}'>Ver Exámenes</a>
                
                <a class='btn btn-info' href='/tests/{{ $test->id }}'>Ver</a>
                @endcan
                @can('update', $test)
                <a class='btn btn-primary' href='/tests/{{ $test->id }}/edit'>Editar</a>
                @endcan
                @can('delete', $test)
                {{ csrf_field() }}
                <input type='button' class='btn btn-danger' value='Borrar'>
                @endcan
                @can('hacer', $test)
                <?php 
                $user = Auth::user();
                $hecho = false;
                foreach($user->dotests()->get() as $dotest){
                    if ($dotest->test_id == $test->id){
                        $hecho = true;

                        ?>
                        <a class='btn btn-info' href='/tests/hecho/{{ $dotest->test_id }}'>Ver</a>
                <?php 
                    }
                }?>
                <?php
                    if($hecho != true){?>
                        <a class='btn btn-success' href='/tests/hacer/{{ $test->id }}'>Hacer</a>
                   <?php }?>        
                @endcan
            </td>
        </tr>
        @endforeach
      </table>
      
        
    </div>
    </div>
<footer class=" footer container-fluid text-center">
  <p>Nacho Villena</p>
  <p>Diego Gimeno</p>
</footer>

<script>
  $(document).ready(function(){
        $(".btn-danger").click(function(event) {
            var td = this.parentNode;
            var tr = td.parentNode;
            var id = tr.childNodes[1];
            id = id.innerHTML;
            $.ajax({
                url: '/tests/'+id,
                type: 'delete',
                data: {'_token': '<?php echo csrf_token(); ?>'},
                success: function(result){
                    $(tr).fadeOut(1000);
                },
                error: function(){
                    console.log('Error');
                }
            });
        
        });
  });
    
</script>

    @stop