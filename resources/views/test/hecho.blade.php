@extends('layouts.app')
    @section('content')
    <?php 
    $test = $dotest->test()->first();
    $doquestions = $dotest->questions()->get();

    
    ?>
    <div class="container-fluid">    
  <div class="row content">
  <div class="col-sm-12">
    <div class="col-sm-10 "> 
      <h1 style="text-align: center;">Exámen de "{{ $test->module->name }}" </h1>
      <?php if (isset($test->criterion)){ ?><p class="criterio" style="font-style: italic; text-align: center;">"(Criterio) {{$test->criterion->name}}"</p><?php }?>
      @if ($test->time != null)
      <p class="criterio" style="font-style: italic; text-align: center;">"Tiempo: {{ $test->time }} min."</p>
      @endif
    </div>
    <div class="col-sm-2">
      <table style="border-collapse: separate; border-spacing:  10px; text-align: center;">
      <tbody style="font-size: 20px; ">
        <tr>
          <th>Acertadas</th><th>Falladas</th><th style="font-size: 30px; ">Nota</th>
        </tr>
        <tr>
          <td>{{ $dotest->acertadas }}</td><td>{{ $dotest->falladas }}</td><td style="font-size: 30px; border: 0.5px solid black;">{{ $nota }}</td>
        </tr>
      </tbody>
      </table>
    </div>
      <div class="col-sm-12">
      <div style="margin-left: 5%;" class="preguntas">
      <ul style="list-style-type: decimal;">
        @foreach($test->questions as $question)
          @foreach($doquestions as $doquestion)
            @if($question->id == $doquestion->id)
        <div class="pregunta">
          <li>
          
          <div class="enunciado">
            <p>Enunciado: <b>{{ $question->wording }}</b></p>
          </div>
          <div class="respuestas">
            <ul style="list-style-type:  lower-latin;" >
              @if($doquestion->pivot->contestada == 'a')
                @if($doquestion->pivot->acertada == 'si')
                  <li style="color: lightgreen">{{ $question->a }} <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></li>
                @else
                  <li style="color: red;">{{ $question->a }} <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></li>
                @endif
              @else
                <li>{{ $question->a }}</li>
              @endif
              @if ($doquestion->pivot->contestada == 'b')
                @if ($doquestion->pivot->acertada == 'si')
                  <li style="color: lightgreen">{{ $question->b }} <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></li>
                @else
                  <li style="color: red;">{{ $question->b }} <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></li>
                @endif
              @else
                <li>{{ $question->b }}</li>
              @endif
              @if ($doquestion->pivot->contestada == 'c')
                @if ($doquestion->pivot->acertada == 'si')
                  <li style="color: lightgreen">{{ $question->c }} <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></li>
                @else
                  <li style="color: red;">{{ $question->c }} <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></li>
                @endif
              @else
                <li>{{ $question->c }}</li>
              @endif
              <?php if ($question->d != null) {
                  if ($doquestion->pivot->contestada == 'd'){
                    if ($doquestion->pivot->acertada == 'si'){
                      echo "<li style='color: lightgreen'> $question->d <span class='glyphicon glyphicon-ok' aria-hidden='true'></span></li>";
                    }else{
                      echo "<li style='color: red;'> $question->d <span class='glyphicon glyphicon-remove' aria-hidden='true'></span></li>";
                    }
                
                }else{
                  echo "<li> $question->d </li>";
                }
              }?>
              
            </ul>
          </div>
          
          </li>
        </div>
            @endif
          @endforeach
        @endforeach
        </ul>
      </div>
      </div>
      </div>
      </div>
      </div>
      @can('create', $test)
        <a class="btn btn-default" style="margin: 1em; margin-left: 40%;" value="Imprimir" href="/tests/pdf/alumnos/{{ $test->id }}">Imprimir</a>
      @endcan
        <footer class=" footer container-fluid text-center">
  <p>Nacho Villena</p>
  <p>Diego Gimeno</p>
</footer>

    @stop