@extends('layouts.app')
    @section('content')
    <div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-10 text-left"> 
      <h1 style="text-align: center;">Exámen de "{{ $test->module->name }}" </h1>
      <?php if (isset($test->criterion)){ ?><p class="criterio" style="font-style: italic; text-align: center;">"(Criterio) {{$test->criterion->name}}"</p><?php }?>
      @if ($test->time != null)
      <p class="criterio" style="font-style: italic; text-align: center;">"Tiempo: {{ $test->time }} min."</p>
      @endif
      <div style="margin-left: 5%;" class="preguntas">
      <ul style="list-style-type: decimal;">
        @foreach($test->questions as $question)
        <div class="pregunta">
          <li>
          
          <div class="enunciado">
            <p>Enunciado: <b>{{ $question->wording }}</b></p>
          </div>
          <div class="respuestas">
            <ul style="list-style-type:  lower-latin;" >
            @can('create', $test)
            @if ($question->solution == 'a')
              <li style="color: green;"><b>{{ $question->a }}</b></li>
            @else
              <li>{{ $question->a }}</li>
            @endif
            @if ($question->solution == 'b')
              <li style="color: green;"><b>{{ $question->b }}</b></li>
            @else
              <li>{{ $question->b }}</li>
            @endif
            @if ($question->solution == 'c')
              <li style="color: green;"><b>{{ $question->c }}</b></li>
            @else
              <li>{{ $question->c }}</li>
            @endif
            @if ($question->solution == 'd')
              <li style="color: green;"><b>{{ $question->d }}</b></li>
            @else
              <?php if ($question->d != null) {
                echo "<li> $question->d </li>";
              }?>
            @endif
            @else
              <li>{{ $question->a }}</li>
              <li>{{ $question->b }}</li>
              <li>{{ $question->c }}</li>
              <?php if ($question->d != null) {
                echo "<li> $question->d </li>";
              }?>
            @endcan
              
            </ul>
          </div>
          
          </li>
        </div>
        @endforeach
        </ul>
      </div>
      </div>
      </div>
      </div>
      @can('create', $test)
        <a class="btn btn-default" style="margin: 1em; margin-left: 40%;" value="Imprimir" href="/tests/pdf/alumnos/{{ $test->id }}">Imprimir</a>
      @endcan
        <footer class=" footer container-fluid text-center">
  <p>Nacho Villena</p>
  <p>Diego Gimeno</p>
</footer>

    @stop