@extends('layouts.app')
    @section('content')
    
    <div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-10 text-left"> 
      <div class="col-md-4"></div><h1 class="col-md-4" style="text-align: center;">Examen de "{{ $test->module->name }}" </h1><h3 class="col-md-4 text-center" id="crono"></h3>
      <p class="criterio col-md-12" style="font-style: italic; text-align: center;">"(Criterio) Cada 2 fallos -0.5p"</p>
      <form id="examen" class="form" action="/tests/comprobar/{{ $test->id }}" method="POST" >
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div style="margin-left: 5%;" class="preguntas" onload="preguntas({{ $test->num_preguntas }})">
      <ul style="list-style-type: decimal;">
      <?php $cont=0;?>
        @foreach($test->questions as $question)
        <?php $cont=$cont+1;?>
        <div class="col-md-4"></div>
        <div class="col-md-4">
        @if ( $cont == 1 )
          <div class="pregunta activa ">
        @else
          <div class="pregunta hide">
        @endif
          <li value="{{ $cont }}">
          <div class="enunciado">
            <p>Enunciado: <b>{{ $question->wording }}</b></p>
          </div>
          <div class="respuestas">
            <ul style="list-style-type:  lower-latin;" >
              <li><input type="radio" name="{{ $question->id }}" value="a"> {{ $question->a }} </li>
              <li><input type="radio" name="{{ $question->id }}" value="b"> {{ $question->b }} </li>
              <li><input type="radio" name="{{ $question->id }}" value="c"> {{ $question->c }} </li>
              <?php if ($question->d != null) {
                echo "<li><input type='radio' name='$question->id' value='d'> $question->d </li>";
              }?>
            </ul>
          </div>
          
          </li>
        </div>
        </div>
        <div class="col-md-4"></div>
        @endforeach
        </ul>
      </div><br><br>
      <ul class="pager col-md-12" id="pager">
        
      </ul>
      <div class="text-center col-md-12" id="finalizar">
        
      </div>
      </form><br><br>
      </div>
      </div>
      </div>
        
        <footer class=" footer container-fluid text-center">
  <p>Nacho Villena</p>
  <p>Diego Gimeno</p>
</footer>

<script type="text/javascript">
  var numero = parseInt("{{ $test->num_preguntas }}");
  <?php if($test->time != null) {?>
  var minutos = parseInt("{{ $test->time }}");
  var segundos = 0;
  <?php }?>
  var actual = 1;
  <?php if($test->time != null){ ?>
 function tiempo(){
    var sMin = "";
    var sSeg = "";
    if ( segundos != -1) {
      segundos--;
    }
    if (segundos == -1 && minutos != 0) {
      segundos = 59;
      minutos--;
    }
    if (minutos == 0 && segundos == 0) {
      var formulario = document.getElementById("examen");
      alert("Fin del exámen");
      formulario.submit();
    }
    sMin = convertirAString(minutos);
    sSeg = convertirAString(segundos);

    document.getElementById("crono").innerHTML = sMin+":"+sSeg;

    setTimeout("tiempo()",1000);
  }
  function convertirAString(num){
    var numero = new String;
    if ( num >=0 && num <=9 ) {
      numero = "0"+num;
    }else{
      numero = ""+num;
    }
    return numero;
  }
  <?php } ?>
  function cargarPaginador(){
    var paginador = document.getElementById("pager");
    paginador.innerHTML = "<li><a href='#' onclick='anterior()'>Anterior</a></li> <li><a href='#' onclick='siguiente()'>Siguiente</a></li>";
    if (actual == 1) {
      paginador.innerHTML = "<li><a href='#' onclick='siguiente()'>Siguiente</a></li>";
    }
    if (actual == numero){
      paginador.innerHTML = "<li><a href='#' onclick='anterior()'>Anterior</a></li>";
    }
    cargarFinalizar();
  }
  function cargarFinalizar(){
    var boton = document.getElementById("finalizar");
      if(actual == numero){
        boton.innerHTML = "<input class='btn btn-success' type='submit' name='Finalizar' value='Finalizar'>";
      }else{
        boton.innerHTML = "";
      }
  }
  function siguiente(){
    actual++;
    var pregunta = document.getElementsByClassName("pregunta");
    var encontrado= false;
    for ( var i=0 ; i<numero ; i++ ){
      
      if ($(pregunta[i]).hasClass('activa') && encontrado == false){
        encontrado = true;
        pregunta[i].setAttribute('class', 'pregunta hide');
        if (i+1 == numero){
          pregunta[i].setAttribute('class', 'pregunta activa');
        }else{
          pregunta[i+1].setAttribute('class', 'pregunta activa');
        } 
      }
    }
    cargarPaginador();
  }
  function anterior(){
    actual--;
    var pregunta = document.getElementsByClassName("pregunta");
    var encontrado= false;
    for ( var i=0 ; i<numero ; i++ ){
      
      if ($(pregunta[i]).hasClass('activa') && encontrado == false){
        encontrado = true;
        pregunta[i].setAttribute('class', 'pregunta hide');
        if (i-1 == numero){
          pregunta[i].setAttribute('class', 'pregunta activa');
        }else{
          pregunta[i-1].setAttribute('class', 'pregunta activa');
        } 
      }
    }
    cargarPaginador();
  }
  $(document).ready(function(){
    cargarPaginador();
    window.addEventListener('load', tiempo(), true);
    
  });
</script>

    @stop