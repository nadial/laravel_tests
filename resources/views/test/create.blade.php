@extends('layouts.app')
    @section('content')
    <div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-10 text-left"> 
      <h4>{{ $study->name }}</h4>
      <h1 style="text-align: center;">Examen de "{{ $module->name }}" </h1>
      <p class="criterio" style="font-style: italic; text-align: center;">"(Criterio) Cada 2 fallos -0.5p"</p>
      <div style="margin-left: 5%;" class="preguntas">
      <ul style="list-style-type: decimal;">
        @foreach($questions as $question)
        <div class="pregunta">
          <li>
          
          <div class="enunciado">
            <p>Enunciado: <b>{{ $question->wording }}</b></p>
          </div>
          <div class="respuestas">
            <ul style="list-style-type:  lower-latin;" >
              <li>{{ $question->a }}</li>
              <li>{{ $question->b }}</li>
              <li>{{ $question->c }}</li>
              <?php if ($question->d != null) {
                echo "<li> $question->d </li>";
              }?>
              
            </ul>
          </div>
          
          </li>
        </div>
        @endforeach
        </ul>
      </div>

       <button style="margin: 1em; margin-left: 40%;" class="btn btn-default">Imprimir</button>
        <footer class=" footer container-fluid text-center">
  <p>Nacho Villena</p>
  <p>Diego Gimeno</p>
</footer>

    @stop