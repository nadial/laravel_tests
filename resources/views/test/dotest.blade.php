@extends('layouts.app')
<meta name="csrf-token" content="{{ csrf_token() }}">
    @section('content')
    <div class="container-fluid text-center">    
  <div class="row content">
  <div class="col-sm-2 sidenav">
      <p><a href="/tests/mostrar"><- Volver</a></p>  
    </div>
    <div class="col-sm-10 text-left"> 
      <h1 class="text-center">Exámen de {{ $test->module->name }}</h1>
      <p><b>ID: </b>{{ $test->id }}</p>
      <p><b>Nombre: </b>{{ $test->name }}</p>
      <p><b>Creado por: </b>{{ $test->user->name }} {{ $test->user->surname }}</p>
      <p><b>Criterio: </b> <?php if (isset($test->criterion)){ ?>{{ $test->criterion->name }} <?php }else{?> Ninguno <?php }?></p>
      <p><b>Tiempo: </b>
      @if ($test->time == null)
            Ilimitado
            @else
            {{ $test->time }} min.
            @endif
      </p>
      <p><b>Número de preguntas: </b>{{ $test->num_preguntas }}</p>
      <table class="table table-condensed">
        <tr>
          <th>Alumno</th><th>Acertadas</th><th>Acciones</th>
        </tr>
        @foreach($test->dotests as $dotest)
        <tr>
            <td>{{ $dotest->user->name }} {{ $dotest->user->surname }}</td>
            <td>{{ $dotest->acertadas }}/{{ $test->num_preguntas }}</td>
            <td>  
                <a class='btn btn-info' href='/dotests/{{ $dotest->id }}'>Ver</a>
                <input type='button' class='btn btn-danger' value='Borrar'>
                
                
            </td>
        </tr>
        @endforeach
      </table>
      
        
    </div>
    </div>
<footer class=" footer container-fluid text-center">
  <p>Nacho Villena</p>
  <p>Diego Gimeno</p>
</footer>

<script>
  $(document).ready(function(){
        $(".btn-danger").click(function(event) {
            var td = this.parentNode;
            var tr = td.parentNode;
            var id = tr.childNodes[1];
            id = id.innerHTML;
            $.ajax({
                url: '/tests/'+id,
                type: 'delete',
                data: {'_token': '<?php echo csrf_token(); ?>'},
                success: function(result){
                    $(tr).fadeOut(2000);
                },
                error: function(){
                    console.log('Error');
                }
            });
        
        });
  });
    
</script>

    @stop