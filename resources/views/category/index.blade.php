@extends('layouts.app')


@section('content')
    <h1>Lista de categorias</h1>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
            </tr>
        </thead>

        <tbody>
        @foreach($categories as $category)
        <tr>
            <td>{{ $category['id'] }}</td>
            <td>{{ $category['name'] }}</td>
            <td>

                <form method="post" action="/categories/{{ $category->id }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Borrar">
                    <a href="/categories/{{ $category->id }}/edit">Editar</a>
                    <a href="/categories/{{ $category->id }}">Ver</a>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
        </table>
    <p><a href="/categories/create">Nuevo</a></p> 
    {!! $categories->render() !!}    
@stop