@extends('layouts.app')
    @section('content')
    <form class="form text-center" action="/modules/{{ $module->id }}" method="post">
    {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
        <label>ID: </label><input type="text" name="id" value="{{ $module->id }}" readonly><br>
        </div>
        <div class="form-group">
        <label>Codigo: </label><input type="text" name="code" value="{{ $module->code }}"><br>
        </div>
        <div class="form-group">
        <label>Nombre: </label><input type="text" name="name" value="{{ $module->name }}"><br>
        </div>
        <input type="submit" name="Editar"><br>
    </form>

    @stop