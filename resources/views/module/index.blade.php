@extends('layouts.app')

<meta name="csrf-token" content="{{ csrf_token() }}">

    @section('content')
    <div class="container-fluid text-center">    
    <div class="row content">
    <?php if (Auth::user()->rol == 'admin') { ?>
    <div class="col-sm-2 sidenav">
      <p><a href="#" data-toggle="modal" data-target="#myModal">Crear Módulo</a></p>
      
    </div>
    <div class="col-sm-10 text-left">
    <?php }else{ ?>
    <div class="col-sm-12">
     <?php } ?>
        <h1 class="text-center">Lista de Módulos</h1>
        <div class="col-sm-6" ></div>
        <div class="col-md-6" >
            <div class="form-group col-md-3">
                <select id='opcion' class="form-control">
                    <option value="name">Nombre</option>
                    <option value="code">Código</option>
                    <option value="shortName">Nom Abreviado</option>
                </select>
            </div>
            <div class="form-group has-feedback col-md-9">
                <input maxlength="30" placeholder="Buscar" class="form-control" type="text" name="name" id="nombreBuscar" "><span class='glyphicon glyphicon-search form-control-feedback'></span>
            </div>  
        </div>
   
        <table class="table table-condensed col-sm-12" id="tablaModules" onload="cargarModules()"> 
        
        </table>
        <div class="col-md-12 text-center">
        <ul class="pagination " id="pagination">

        </ul>
        </div>
        </div>
        </div>
    </div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear Módulo</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="/modules">
        {{ csrf_field() }}
            <div class="form-group">
                <label class="control-label col-sm-2" for="name">Nombre: </label>
                <div class="col-sm-10">
                <input class="form-control type="text" name="name">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="code">Codigo: </label>
                <div class="col-sm-10">
                <input class="form-control type="text" name="code">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="shortName">Nom. Abreviado: </label>
                <div class="col-sm-10">
                <input class="form-control type="text" name="shortName">
                </div>
            </div>
            <div class="form-group text-center">
            <input class="btn btn-success" type="submit" name="Crear" value="Crear">
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Añadir modulo a un estudio</h4>
      </div>
      <div class="modal-body">
        <form id="form" class="form-horizontal" method="POST" action="/modules/anadir">
        {{ csrf_field() }}
            <div class="form-group">
                <label class="control-label col-sm-2" for="name">Estudio: </label>
                <div class="col-sm-10">
                <select id="estudios" name="estudio">
                    
                </select>
                </div>
            </div>
            
            <div class="form-group text-center">
            <input class="btn btn-success" type="submit" name="Crear" value="Añadir">
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
    var nombre;
    var opcion;
    var actualPage;
    var maxPage;
    var editado = false;
    
    function paginador(){
        var ul = document.getElementById("pagination");
        ul.innerHTML = "";
        if (maxPage != 1){
        for (var i = 1; i <=maxPage; i++ ){
            var li = document.createElement("li");
            
            if (actualPage == i) {
                li.setAttribute('class', 'active');
            }

            li.innerHTML = "<a href='#' onclick='cargarModules("+i+")'>"+i+"</a>";
            ul.appendChild(li);
        }
    }
    }
    function paginadorBuscador(){
        var ul = document.getElementById("pagination");
        ul.innerHTML = "";
        if (maxPage != 1){
        for (var i = 1; i <=maxPage; i++ ){
            var li = document.createElement("li");
            
            if (actualPage == i) {
                li.setAttribute('class', 'active');
            }

            li.innerHTML = "<a href='#' onclick='mostrarModule("+i+")'>"+i+"</a>";
            ul.appendChild(li);
        }
    }
    }
    function abrir(id) {
        var select = document.getElementById("estudios");
        var form = document.getElementById("form");
        form.setAttribute("action", '/modules/anadir/'+id);
        select.innerHTML = "";
        $.ajax({
                url: '/modules/studies',
                type: 'GET',
                data: {'module_id': id},
                success: function(result){
                    var studies = JSON.parse(result);
                    for (x in studies){
                        var option = document.createElement("option");
                        option.setAttribute("value", studies[x].id);
                        option.innerHTML = studies[x].name;   
                        select.appendChild(option);        
                    }
                },
                error: function(){
                    console.log('Error');
                }
            });
    }

    function cargarModules(page){
        var xhttp;
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var tabla = document.getElementById("tablaModules");
                    tabla.innerHTML = "";
                    var tbody = document.createElement("tbody");
                    tabla.appendChild(tbody);
                    var modules = JSON.parse(this.responseText);
                    maxPage = modules.last_page;
                    actualPage = modules.current_page;
                    var tr = document.createElement("tr");
                    tr.innerHTML = "<th>ID</th><th>Nombre</th><th>Código</th><th>Nom. Abreviado</th><th>Acción</th>";
                    tbody.appendChild(tr);     
                        for (x in modules.data) {
                            var tr = document.createElement("tr");
                            tr.setAttribute('id', 'module');
                            tr.innerHTML += "<td>"+modules.data[x].id+"</td><td>"+modules.data[x].name+"</td><td>"+modules.data[x].code+"</td><td>"+modules.data[x].shortName+"</td><td> <a class='btn btn-info' href='/modules/"+modules.data[x].id+"'>Ver</a> <?php if (Auth::user()->rol == 'admin') { ?><a class='btn btn-primary editar' href='#'>Editar</a> <input type='button' class='btn btn-danger' value='Borrar'><button onclick='abrir("+modules.data[x].id+")' class='btn btn-success' data-toggle='modal' data-target='#myModal2'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></button><?php } ?></td>"; 
                            tbody.appendChild(tr);
                        }
                        paginador();
                        borrarModule();
                        editarModule();
                        editado = false;
                }
                if (this.readyState == 4 && this.status == 404){
                    document.getElementById('resultado').innerHTML = "No se han encontrado ningun estudio";
                }
            };
            xhttp.open("GET", "/modules/ajax?page="+page, true);
            xhttp.send();  
            }
        function borrarModule(){
        $(".btn-danger").click(function(event) {
            var td = this.parentNode;
            var tr = td.parentNode;
            var id = tr.firstChild;
                
            id = id.innerHTML;
            $.ajax({
                url: '/modules/'+id,
                type: 'delete',
                data: {'_token': '<?php echo csrf_token(); ?>'},
                success: function(result){
                    $(tr).fadeOut(2000);
                },
                error: function(){
                    console.log('Error');
                }
            });
        
        });
    }
    function editarModule(){
        $(".editar").click(function(event) {
            if (editado == false) {
            var td = this.parentNode;
            var tr = td.parentNode;
            var id = tr.childNodes[0].innerHTML
            var nombre = tr.childNodes[1].innerHTML
            var codigo = tr.childNodes[2].innerHTML
            var nomAbre = tr.childNodes[3].innerHTML
            tr.childNodes[1].innerHTML = "<input class='inputEdit' type='text' name='name' value='"+nombre+"'>";
            tr.childNodes[2].innerHTML = "<input class='inputEdit' type='text' name='code' value='"+codigo+"'>";
            tr.childNodes[3].innerHTML = "<input class='inputEdit' type='text' name='shortName' value='"+nomAbre+"'>";
            var td4 = tr.childNodes[4];
            var editar = td4.childNodes[3];
            editar.setAttribute('class', 'btn btn-success editable');
            editar.setAttribute('onclick', 'moduleEditado(this)');
            editar.innerHTML = "Aceptar";
            editado = true;
        }
            cambiarInput();
        });

    }
    function cambiarInput(){
        $(".inputEdit").keyup(function(event) {
               var valor = this.value;
               this.setAttribute('value', valor);
            });
    }
    function moduleEditado(elemento){
        var td = elemento.parentNode;
        var tr = td.parentNode;
        tr.setAttribute('style', 'background-color: white;');
        var id = tr.childNodes[0].innerHTML;
        var inputNombre = tr.childNodes[1];
        var nombre = inputNombre.firstChild.value;
        var inputCodigo = tr.childNodes[2];
        var codigo = inputCodigo.firstChild.value;
        var inputNomAbre = tr.childNodes[3];
        var nomAbre = inputNomAbre.firstChild.value;
        var td4 = tr.childNodes[4];
        var editar = td4.childNodes[3];
        $.ajax({
                url: '/modules/'+id,
                type: 'put',
                data: {'_token': '<?php echo csrf_token(); ?>', 'name': nombre, 'code': codigo, 'shortName': nomAbre},
                success: function(result){
                    editado = false;
                    inputNombre.innerHTML = nombre;
                    inputCodigo.innerHTML = codigo;
                    inputNomAbre.innerHTML = nomAbre;
                    editar.setAttribute('class', 'btn btn-primary editar');
                    editar.setAttribute('onclick', '');
                    editar.innerHTML = "Editar";
                    $(tr).animate({opacity: '0.4'}, "slow");
                    $(tr).animate({opacity: '1'}, "slow");
                },
                error: function(){
                    console.log('Error');
                }
            });


    }
    

    function mostrarModule(page) {
            var xhttp;
            var html ="";
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var tabla = document.getElementById("tablaModules");
                    tabla.innerHTML = "";
                    var tbody = document.createElement("tbody");
                    tabla.appendChild(tbody);
                    var modules = JSON.parse(this.responseText);
                    maxPage = modules.last_page;
                    actualPage = modules.current_page;
                    var tr = document.createElement("tr");
                    tr.innerHTML = "<th>ID</th><th>Nombre</th><th>Código</th><th>Nom. Abreviado</th><th>Acción</th>";
                    tbody.appendChild(tr);     
                        for (x in modules.data) {
                            var tr = document.createElement("tr");
                            tr.innerHTML = "<td>"+modules.data[x].id+"</td><td>"+modules.data[x].name+"</td><td>"+modules.data[x].code+"</td><td>"+modules.data[x].shortName+"</td><td> <a class='btn btn-info' href='/modules/"+modules.data[x].id+"'>Ver</a> <?php if (Auth::user()->rol == 'admin') { ?><a class='btn btn-primary' href='/modules/"+modules.data[x].id+"/edit'>Editar</a> <input type='button' class='btn btn-danger' value='Borrar'><?php } ?></td>"; 
                            tbody.appendChild(tr);
                        }
                        paginadorBuscador();
                        
                }
                if (this.readyState == 4 && this.status == 404){
                    cargarModules();
                }
            };
            xhttp.open("GET", "/modules/buscar/"+opcion+"/"+nombre+"?page="+page, true);
            xhttp.send();  
            }
</script>

<script>
    $(document).ready(function(){
        cargarModules();
        $("#nombreBuscar").keyup(function(){
            nombre = document.getElementById('nombreBuscar').value;
            opcion = document.getElementById('opcion').value;
            mostrarModule();
        });
        
    });
</script>

    @stop