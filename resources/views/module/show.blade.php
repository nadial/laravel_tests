@extends('layouts.app')
    @section('content')
    <div class="jumbotron text-center">
        <h1> Detalles de "{{ $module->name }}" </h1>
    </div>
    <div>
        <div class="text-center">
        <p><b>Id:</b> {{$module->id}}</p>
        <p><b>Código:</b> {{$module->code}}</p>
        <p><b>Abreviatura:</b> {{$module->shortName}}</p>
        <?php if(count($module->tests) != 0){ ?>
        <h3>Exámenes</h3>
        </div>
        <table class="table table-condensed">
            <tr>
                <th>Id</th><th>Nombre</th><th>Num.Preguntas</th><th>Creado por</th><th>Acciones</th>
            </tr>
        @foreach($module->tests as $test)
        <tr>
            <td>{{ $test->id }}</td>
            <td>{{ $test->module->name }}</td>
            <td>{{ $test->num_preguntas }}</td>
            <td>{{ $test->user->name }} {{ $test->user->surname }}</td>
            
            <td>
                @can('delete', $test)
                <form method="post" action="/tests/{{ $test->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="Borrar">
                @endcan
                @can('update', $test)
                <a href="/tests/{{ $test->id }}/edit">Editar</a>
                @endcan
                <a href="/tests/{{ $test->id }}">Ver</a>
                </form>
            </td>
            
        </tr>
        @endforeach
        </table>
        <?php  }?>
        <div class="text-center">
            <a class="btn btn-default" href="/modules"><-Volver</a>
        </div>
    </div>

    @stop