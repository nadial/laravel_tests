@extends('layouts.app')
    @section('content')
    <div class="container-fluid text-center">    
  <div class="row content">
  <div class="col-sm-2 sidenav">
      <p><a href="/tests"><- Volver</a></p> 
    </div>
    <div class="col-sm-10 text-left"> 
      <h1 style="text-align: center;">Crear Pregunta </h1>
      <form class="form" method="post" action="/questions">
        {{ csrf_field() }}
            <div class="form-group">
            <label for="sel1">Estudio: </label>
            <select class="form-control" name="study" onchange="mostrarmodulos(this.value)">
                <option value="0">--Selecciona Estudio--</option>
                @foreach($studies as $study)
                    <option value="{{ $study->id }}">{{ $study->name }}</option>
                @endforeach
            </select>
            </div>
            <div class="form-group">
            <label for="sel1">Modulo: </label>
            <select class="form-control" name="module" id="modules">
            </select>
            </div>
            <div class="form-group">
            <label for="sel1">Categoria: </label>
            <select class="form-control" name="category">
                <option value="0">--Selecciona Categoria--</option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            </div>
            <div class="form-group">
            <label for="sel1">Enunciado: </label>
            <textarea class="form-control" rows="5" name="wording" id="comment"></textarea>
            </div>
            <h4>Elige la correcta:</h4>
            <div class="form-group">
            <label for="sel1">Opcion a: <input type="radio" value="a" name="solution"></label>
            <input class="form-control" type="text" name="a">
            </div>
            <div class="form-group">
            <label for="sel1">Opcion b: <input type="radio" value="b" name="solution"></label>
            <input class="form-control" type="text" name="b">
            </div>
            <div class="form-group">
            <label for="sel1">Opcion c: <input type="radio" value="c" name="solution"></label>
            <input class="form-control" type="text" name="c">
            </div>
            <div class="form-group">
            <label for="sel1" id="labelD">Opcion d ('No obligatoria'): </label>
            <input class="form-control" type="text" name="d" id="d">
            </div>
  
            <div class="form-group text-center">
                <input class="btn btn-success" type="submit" name="Crear" value="Crear">
            </div>
      </form>
 <script>
            function mostrarmodulos(id) {
            var xhttp;
            var html ="";
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var modules = JSON.parse(this.responseText);
                        for (x in modules) {
                            html += "<option value='"+modules[x].id+"'>"+modules[x].name+"</option>";
                        }
                    
                document.getElementById('modules').innerHTML = html;
                }
                if (this.readyState == 4 && this.status == 500){
                    html += "<option>--Nada--</option>";
                    document.getElementById('modules').innerHTML = html;
                }
            };
            xhttp.open("GET", "/tests/ajax/"+id, true);
            xhttp.send();  
            }
</script>
<script>
    $(document).ready(function(){
        $("#d").keyup(function(){
            d = document.getElementById('d').value;

            if (d == '') {
              document.getElementById('labelD').innerHTML = "Opcion d ('No obligatoria'): ";
            }else{
              document.getElementById('labelD').innerHTML = "Opcion d ('No obligatoria'): <input type='radio' value='d' name='solution'> ";
            }
        });
    });
</script>
    @stop